if test $# -ne 1
then
    echo "Eroare numar de argumente primite"
    exit 1
fi

chmod +r "$1"

result=0
safe="SAFE"

i=0
linii=0
cuvinte=0
caractere=0

for entity in $(wc "$1")
do

    if test "$i" -eq 0
    then 
        linii=$entity
        i=1
    elif test "$i" -eq 1
    then 
        cuvinte=$entity
        i=2
    elif test "$i" -eq 2
    then 
        caractere=$entity
        i=3
    fi
done

if [ "$linii" -lt 3 -o "$cuvinte" -gt 1000 -o "$caractere" -gt 2000 ]
then
    result=1
fi

if grep -q "corrupted" "$1" 
then
    result=1
fi

if grep -q "dangerous" "$1"
then
    result=1
fi

if grep -q "risk" "$1" 
then
    result=1
fi

if grep -q "attack" "$1" 
then
    result=1
fi

if grep -q "malware" "$1" 
then
    result=1
fi

if grep -q "malicious" "$1" 
then
    result=1
fi

if grep -q "[0x80-0xFF]" "$1"
then
    result=1
fi

chmod -r "$1"

if test $result -eq 1
then
    echo "$1"
else
    echo "$safe"
fi

exit $result