#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

void citesteDirector(DIR* director){
    struct dirent* aux;

    while(aux = readdir(director)){
        if(strcmp(aux->d_name,".") && strcmp(aux->d_name,"..")){
            if(aux->d_type == DT_DIR){
                printf("%s : ",aux->d_name);
                DIR* d;
                if(!(d = opendir(aux->d_name))){
                    fprintf(stderr,"Nu am putut deschide un director interior.\n");
                    exit(-1);
                }
                citesteDirector(d);
            }
            printf("%s\n",aux->d_name);
        }        
    }
    if(errno != 0){
        fprintf(stderr,"Eroare la citirea din director.\n");
        exit(-1);
    }
}

int main(int argc, char* argv[]){
    if(argc != 2){
        fprintf(stderr,"Nu ati apelat cu 2 argumente!\n");
        exit(-1);
    }

    DIR* director;
    if(!(director = opendir(argv[1]))){
        fprintf(stderr,"Calea catre director nu este corecta/directorul nu s-a putut deschide.\n");
        exit(-1);
    }

    citesteDirector(director);

    return 0;
}