#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#define MAX_SIZE_FISIER 1000
#define LUNGIME_NUME 100

typedef struct{
    char nume[256];
    char modt[256];
}Informatii;

int deschideFisier(char* cale){
    int f;
    if((f = open(cale,O_RDWR | O_CREAT, S_IRWXU)) == -1){
        fprintf(stderr,"Eroare la deschiderea sau crearea fisierului %s.\n",cale);
        exit(-1);
    }

    return f;
}

DIR* deschideDirector(char* cale){
    DIR* d;
    if(!(d = opendir(cale))){
        fprintf(stderr,"Calea catre director nu este corecta/directorul nu s-a putut deschide.\n");
        exit(-1);
    }

    return d;
}

int verificaFisierGol(char* cale){
    struct stat st;
    if(lstat(cale,&st) == -1){
        fprintf(stderr,"Eroare de lstat la verificarea fisierului gol.\n");
        exit(-1);
    }
    else{
        if(st.st_size == 0){
            return 1;
        }
    }
    return 0;
}

void citesteDirector(DIR* director, char* cale, Informatii* info, int* size){
    struct dirent* aux;

    while((aux = readdir(director))){
        if(strcmp(aux->d_name,".") && strcmp(aux->d_name,"..")){
            if(aux->d_type == DT_DIR){
                DIR* d;
                char* nume = malloc(sizeof(char)*(strlen(cale)+1+strlen(aux->d_name)));
                strcpy(nume,cale);
                strcat(nume,"/");
                strcat(nume,aux->d_name);

                d = opendir(nume);
                if(!d){
                    fprintf(stderr,"Nu am putut deschide un director interior.\n");
                    exit(-1);
                }
                citesteDirector(d,nume,info,size);
                free(nume);
            }
            else{
                char* caleF = malloc(sizeof(char)*(strlen(cale)+1+strlen(aux->d_name)));
                strcpy(caleF,cale);
                strcat(caleF,"/");
                strcat(caleF,aux->d_name);

                struct stat st;
                if(lstat(caleF,&st) == -1){
                    fprintf(stderr,"Eroare de lstat.\n");
                    exit(-1);
                }
                *size = *size+1;
                strcpy(info[*size - 1].nume,aux->d_name);
                char auxTime[256];
                sprintf(auxTime, "%ld", st.st_mtime);
                strcpy(info[*size - 1].modt,auxTime);

                free(caleF);
            }
        }        
    }
    if(errno != 0){
        fprintf(stderr,"Eroare la citirea din director.\n");
        exit(-1);
    }
}

int verificaSchimbare(int fd, char* buffer, int count){
    char* bufferF = malloc(sizeof(char) * count);
    int verif = read(fd,bufferF,count);

    if(verif == -1){
        fprintf(stderr,"Eroare de citire din file descriptor.\n");
        exit(-1);
    }
    else{
        if(verif == count){
            verif = read(fd,NULL,1);
            if(strcmp(buffer,bufferF) && !verif){
                return 1;
            }
        }
    }
    
    return 0;
}

int main(int argc, char* argv[]){
    if(argc == 1){
        fprintf(stderr,"Programul nu a primit calea catre niciun director.\n");
        exit(-1);
    }
    else{
        //aflu directorul in care vor fi adaugate snapshot-urile
        int start = 1;
        char output[LUNGIME_NUME];
        strcpy(output,"");
        if(!strcmp(argv[1],"-o")){
            start = 3;
            strcpy(output,argv[2]);
        }

        for(int i = start; i < argc; i++){ //pentru fiecare director in parte
            //if care verifica -s pentru cerinta urmatoare

            int pid = fork();
            if(pid == -1){
                fprintf(stderr,"Eroare creare proces.\n");
                exit(-1);
            }

            if(pid == 0){
                int VariabilaModificare = 0;

                DIR* director = deschideDirector(argv[i]); //calea catre director
                char cale[2 * LUNGIME_NUME + 4];
                strcpy(cale,output);
                if(strcmp(output,"")){
                    strcat(cale,"/");
                }
                strcat(cale,argv[i]);
                strcat(cale,".txt");

                int fd = deschideFisier(cale);

                //initializez structura care memoreaza informatiile citite
                Informatii* info = malloc(sizeof(Informatii)*MAX_SIZE_FISIER);
                if(!info){
                    fprintf(stderr,"Eroare de alocare a memoriei.\n");
                    exit(-1);
                }
                int* size = malloc(sizeof(int));
                if(!size){
                    fprintf(stderr,"Eroare de alocare a memoriei.\n");
                    exit(-1);
                }
                *size = 0;

                //citesc datele din director
                citesteDirector(director,argv[i],info,size);
                info = realloc(info,sizeof(Informatii)*(*size));
                if(!info){
                    fprintf(stderr,"Eroare de alocare a memoriei.\n");
                    exit(-1);
                }
            
                //imi creez un buffer de cautare in fisier, pe care il voi folosi si pentru a scrie datele in fisier la final
                char* buffer = malloc(sizeof(char) * 4096);
                strcpy(buffer,"");
                for(int k = 0; k < *size; k++){
                    strcat(buffer,info[k].nume);
                    strcat(buffer," ");
                    strcat(buffer,info[k].modt);
                    strcat(buffer,"\n");
                }

                //daca fisierul nu este gol, verific daca au aparut modificari si afisez rezultatul
                //daca fisierul este gol, memorez datele curente despre director in fisier si trec mai departe; se poate spune ca directorul nu a fost modificat
                if(!verificaFisierGol(cale)){  
                    VariabilaModificare += verificaSchimbare(fd,buffer,strlen(buffer));
                    if(VariabilaModificare){
                        printf("Directorul %s a fost modificat.\n",argv[i]);
                    }
                    else{
                        printf("Directorul %s nu a fost modificat.\n",argv[i]);
                    }
                }
                else{
                    printf("S-a creat snapshot pentru directorul %s.\n",argv[i]);
                }

                //la final, ma asigur ca modific datele actualizate pentru fiecare fisier

                fd = deschideFisier(cale);
                int verif = write(fd,buffer,strlen(buffer));
                if(verif != strlen(buffer)){
                    fprintf(stderr,"Eroare de scriere in fisierul %s",cale);
                    exit(-1);
                }

                exit(0);
            }
        }
        
        int end;
        if(start != 1){
            end = argc - start + 1;
            start = 1;
        }
        int status;
        for (int i = start; i < end; i++) {
            int waitValue = wait(&status);
            printf("Procesul copil %d cu PID-ul %d s-a incheiat cu codul de iesire %d.\n",i,waitValue,status);
        }
    }

    return 0;
}
