#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#define MAX_SIZE_FISIER 1000
#define LUNGIME_NUME 100
#define MAX_DATE 4096

typedef struct{
    char nume[256];
    char modt[256];
}Informatii;

int deschideFisier(char* cale){
    int f;
    if((f = open(cale,O_RDWR | O_CREAT, S_IRWXU)) == -1){
        fprintf(stderr,"Eroare la deschiderea sau crearea fisierului %s.\n",cale);
        exit(-1);
    }

    return f;
}

DIR* deschideDirector(char* cale){
    DIR* d;
    if(!(d = opendir(cale))){
        fprintf(stderr,"Calea catre director nu este corecta/directorul nu s-a putut deschide.\n");
        exit(-1);
    }

    return d;
}

int verificaFisierGol(char* cale){
    struct stat st;
    if(lstat(cale,&st) == -1){
        fprintf(stderr,"Eroare de lstat la verificarea fisierului gol.\n");
        exit(-1);
    }
    else{
        if(st.st_size == 0){
            return 1;
        }
    }
    return 0;
}

int citesteDirector(DIR* director, char* cale, Informatii* info, int* size, char izolated_directory[LUNGIME_NUME]){
    struct dirent* aux;
    int i = 0;

    while((aux = readdir(director))){
        if(strcmp(aux->d_name,".") && strcmp(aux->d_name,"..")){
            if(aux->d_type == DT_DIR){
                DIR* d;
                char* nume = malloc(sizeof(char)*(strlen(cale)+1+strlen(aux->d_name)));
                strcpy(nume,cale);
                strcat(nume,"/");
                strcat(nume,aux->d_name);

                d = opendir(nume);
                if(!d){
                    fprintf(stderr,"Nu am putut deschide un director interior.\n");
                    exit(-1);
                }
                i += citesteDirector(d,nume,info,size,izolated_directory);
                free(nume);
            }
            else{
                char* caleF = malloc(sizeof(char)*(strlen(cale)+1+strlen(aux->d_name)));
                if(!caleF){
                    fprintf(stderr,"Eroare de alocare.\n");
                    exit(-1);
                }
                strcpy(caleF,cale);
                strcat(caleF,"/");
                strcat(caleF,aux->d_name);

                struct stat st;
                if(lstat(caleF,&st) == -1){
                    fprintf(stderr,"Eroare de lstat.\n");
                    exit(-1);
                }

                int safeVariable = 1;

                if(strcmp(izolated_directory,"")){
                    //verificare permisiuni
                    if(!(st.st_mode & S_IRWXU) && !(st.st_mode & S_IRWXG) && !(st.st_mode & S_IRWXO)){
                        //verificare fisier malitios

                        safeVariable = 0;

                        int pfd[2];
                        if(pipe(pfd) < 0){
                            fprintf(stderr,"Eroare la functia pipe.\n");
                            exit(-1);
                        }
                        
                        int pid;
                        if((pid = fork()) < 0){
                            fprintf(stderr,"Eroare de fork la verificarea de fisiere malitioase.\n");
                            exit(-1);
                        }
                        if(pid == 0){
		                    close(pfd[0]);
                            
		                    dup2(pfd[1],1);

                            execlp("/bin/sh", "sh", "/home/daniel/Linux/Programare/SO/pb1/verifMalitiousFolder.sh", caleF, NULL); //aici al 3-lea argument trebuie sa fie calea completa catre shell script, restul argumentelor nu trebuie modificate

		                    fprintf(stderr,"Eroare la executarea script-ului\n");
                            exit(-1);
	                    }
                        else{
                            close(pfd[1]);
                            
                            char buffer[strlen(caleF)];
                            int bytes;
                            if((bytes = read(pfd[0],&buffer,sizeof(buffer))) != -1){     
                                buffer[bytes] = '\0';    
                                if(!strcmp(buffer,caleF)){
                                    //fisier unsafe
                                    char* newPath = malloc(sizeof(char)*(strlen(izolated_directory) + strlen(caleF)));
                                    if(!newPath){
                                        fprintf(stderr,"Eroare de alocare.\n");
                                        exit(-1);
                                    }
                                    strcpy(newPath,izolated_directory);
                                    strcat(newPath,"/");
                                    strcat(newPath,aux->d_name);

                                    if(rename(caleF,newPath) < 0){
                                        fprintf(stderr,"Eroare de mutare a fisierului malitios.\n");
                                        exit(-1);
                                    }

                                    free(newPath);
                                }
                                else{
                                    buffer[strlen(buffer) - 1] = '\0';
                                    if(!strcmp(buffer,"SAFE")){ //script-ul returneaza SAFE
                                        safeVariable = 1;
                                    }
                                    else{
                                        fprintf(stderr,"Citire invalida din pipe, buffer este %s\n",buffer);
                                        exit(-1);
                                    }
                                }
                            }
                            else{
                                fprintf(stderr,"Eroare de citire din pipe\n");
                                exit(-1);
                            }

                            close(pfd[0]);

                            int status;
                            wait(&status);
                        }
                    }
                }

                if(safeVariable){
                    *size = *size+1;
                    strcpy(info[*size - 1].nume,aux->d_name);
                    char auxTime[256];
                    sprintf(auxTime, "%ld", st.st_mtime);
                    strcpy(info[*size - 1].modt,auxTime);
                }
                else{
                    i++;
                }

                free(caleF);
            }
        }        
    }
    if(errno != 0){
        fprintf(stderr,"Eroare la citirea din director.\n");
        exit(-1);
    }

    return i;
}

int verificaSchimbare(int fd, char* buffer, int count){
    char* bufferF = malloc(sizeof(char) * count);
    int verif = read(fd,bufferF,count);

    if(verif == -1){
        fprintf(stderr,"Eroare de citire din file descriptor.\n");
        exit(-1);
    }
    else{
        if(verif == count){
            verif = read(fd,NULL,1);
            if(strcmp(buffer,bufferF) && !verif){
                return 1;
            }
        }
    }
    
    return 0;
}

int main(int argc, char* argv[]){
    if(argc == 1){
        fprintf(stderr,"Programul nu a primit calea catre niciun director.\n");
        exit(-1);
    }
    else{
        //aflu directorul in care vor fi adaugate snapshot-urile
        int start = 1;
        
        char output[LUNGIME_NUME];
        strcpy(output,"");
        if(!strcmp(argv[start],"-o")){
            if(argc-start < 3){
                fprintf(stderr,"Nu s-a apelat corespunzator folosind directorul de Output.\n");
                exit(-1);
            }
            
            strcpy(output,argv[start + 1]);
            start += 2;
        }

        //directorul in care izolam fisierele malitioase
        int MalitiousOption = 0;
        char izolated_directory[LUNGIME_NUME];
        strcpy(izolated_directory,"");
        if(!strcmp(argv[start],"-s")){
            if(argc - start < 3){
                fprintf(stderr,"Nu s-a apelat corespunzator folosind directorul de fisiere malitioase.\n");
                exit(-1);
            }
            
            MalitiousOption = 1;
            strcpy(izolated_directory,argv[start+1]);
            start += 2;
        }

        int* count_Fmalitios = malloc(sizeof(int) * (argc-start));
        if(!count_Fmalitios){
            fprintf(stderr,"Eroare de alocare a memoriei.\n");
            exit(-1);
        }
        for(int i = 0; i < argc - start; i++){
            count_Fmalitios[i] = 0;
        }
        int contorMalitios = 0;

        for(int i = start; i < argc; i++){ //pentru fiecare director in parte
            int pfd[2]; //pipe de comunicare cu procesul copil
            if(pipe(pfd) < 0){
                fprintf(stderr,"Eroare de pipe in main.\n");
                exit(-1);
            }
            
            int pid = fork(); //procesul copil
            if(pid == -1){
                fprintf(stderr,"Eroare creare proces.\n");
                exit(-1);
            }
            if(pid == 0){
                int VariabilaModificare = 0;

                DIR* director = deschideDirector(argv[i]); //calea catre director

                char cale[2 * LUNGIME_NUME + 4];
                strcpy(cale,output);
                if(strcmp(output,"")){
                    strcat(cale,"/");
                }
                strcat(cale,argv[i]);
                strcat(cale,".txt");

                int fd = deschideFisier(cale);

                //initializez structura care memoreaza informatiile citite
                Informatii* info = malloc(sizeof(Informatii)*MAX_SIZE_FISIER);
                if(!info){
                    fprintf(stderr,"Eroare de alocare a memoriei.\n");
                    exit(-1);
                }
                int* size = malloc(sizeof(int));
                if(!size){
                    fprintf(stderr,"Eroare de alocare a memoriei.\n");
                    exit(-1);
                }
                *size = 0;

                //citesc datele din director
                int count = citesteDirector(director,argv[i],info,size,izolated_directory);
                if(*size == 0){
                    printf("fisierul %s este gol\n",argv[i]);
                }
                else{
                    info = realloc(info,sizeof(Informatii)*(*size));
                    if(!info){
                        fprintf(stderr,"Eroare de alocare a memoriei.\n");
                        exit(-1);
                    }
            
                    //imi creez un buffer de cautare in fisier, pe care il voi folosi si pentru a scrie datele in fisier la final
                    char* buffer = malloc(sizeof(char) * MAX_DATE);
                    strcpy(buffer,"");
                    for(int k = 0; k < *size; k++){
                        strcat(buffer,info[k].nume);
                        strcat(buffer," ");
                        strcat(buffer,info[k].modt);
                        strcat(buffer,"\n");
                    }

                    //daca fisierul nu este gol, verific daca au aparut modificari si afisez rezultatul
                    //daca fisierul este gol, memorez datele curente despre director in fisier si trec mai departe; se poate spune ca directorul nu a fost modificat
                    if(!verificaFisierGol(cale)){  
                        VariabilaModificare += verificaSchimbare(fd,buffer,strlen(buffer));
                        if(VariabilaModificare){
                            printf("Directorul %s a fost modificat.\n",argv[i]);
                        }
                        else{
                            printf("Directorul %s nu a fost modificat.\n",argv[i]);
                        }
                    }
                    else{
                        printf("S-a creat snapshot pentru directorul %s.\n",argv[i]);
                    }

                    //la final, ma asigur ca modific datele actualizate pentru fiecare fisier

                    fd = deschideFisier(cale);
                    int verif = write(fd,buffer,strlen(buffer));
                    if(verif != strlen(buffer)){
                        fprintf(stderr,"Eroare de scriere in fisierul %s",cale);
                        exit(-1);
                    }

                }
                
                //transmit prin pipe valoarea lui count care a numarat fisierele malitioase gasite
                close(pfd[0]);
                if(write(pfd[1],&count,sizeof(count)) < 0){
                    fprintf(stderr,"Eroare de scriere in pipe in functia main.\n");
                    exit(-1);
                }
                close(pfd[1]);

                exit(0);
            }
            else{
                close(pfd[1]);

                int rez;
                if(read(pfd[0],&rez,sizeof(rez)) < 0){
                    fprintf(stderr,"Eroare de citire din pipe in functia main.\n");
                    exit(-1);
                }
                count_Fmalitios[contorMalitios] = rez;
                
                close(pfd[0]);
            }
            contorMalitios++;
        }

        int end = argc;
        if(start != 1){
            end = argc - start + 1;
            start = 1;
        }

        int status;
        if(MalitiousOption){
            for (int i = start; i < end; i++) {
                int waitValue = wait(&status);
                printf("Procesul copil %d s-a incheiat cu PID-ul %d si cu %d fisiere cu potential periculos.\n",i,waitValue,count_Fmalitios[i]);
            }
        }
        else{
            for (int i = start; i < end; i++) {
                int waitValue = wait(&status);
                printf("Procesul copil %d s-a incheiat cu PID-ul %d si cu statusul %d.\n",i,waitValue,status);
            }
        }
        

        free(count_Fmalitios);
    }

    return 0;
}
