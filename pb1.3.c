#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#define MAX_SIZE_FISIER 1000
#define LUNGIME_NUME 100

typedef struct{
    char nume[256];
    char modt[256];
}Informatii;

int deschideFisier(char* cale){
    int f;
    if((f = open(cale,O_RDWR | O_CREAT, S_IRWXU)) == -1){
        fprintf(stderr,"Eroare la deschiderea sau crearea fisierului %s.\n",cale);
        exit(-1);
    }

    return f;
}

DIR* deschideDirector(char* cale){
    DIR* d;
    if(!(d = opendir(cale))){
        fprintf(stderr,"Calea catre director nu este corecta/directorul nu s-a putut deschide.\n");
        exit(-1);
    }

    return d;
}

int verificaFisierGol(char* cale){
    struct stat st;
    if(lstat(cale,&st) == -1){
        fprintf(stderr,"Eroare de lstat la verificarea fisierului gol.\n");
        exit(-1);
    }
    else{
        if(st.st_size == 0){
            return 1;
        }
    }
    return 0;
}

void citesteDirector(DIR* director, char* cale, Informatii* info, int* size){
    struct dirent* aux;

    while((aux = readdir(director))){
        if(strcmp(aux->d_name,".") && strcmp(aux->d_name,"..")){
            if(aux->d_type == DT_DIR){
                DIR* d;
                char* nume = malloc(sizeof(char)*(strlen(cale)+1+strlen(aux->d_name)));
                strcpy(nume,cale);
                strcat(nume,"/");
                strcat(nume,aux->d_name);

                d = opendir(nume);
                if(!d){
                    fprintf(stderr,"Nu am putut deschide un director interior.\n");
                    exit(-1);
                }
                citesteDirector(d,nume,info,size);
            }
            else{
                struct stat st;
                if(fstat(aux->d_name,&st) == -1){
                    fprintf(stderr,"Eroare de fstat.\n");//verific cu file descriptor, aici e o problema
                    exit(-1);
                }
                *size = *size+1;
                strcpy(info[*size - 1].nume,aux->d_name);
                char aux[256];
                sprintf(aux, "%ld", st.st_mtime);
                strcpy(info[*size - 1].modt,aux);
            }
        }        
    }
    if(errno != 0){
        fprintf(stderr,"Eroare la citirea din director.\n");
        exit(-1);
    }
}

int verificaSchimbare(Informatii* info, int fd, int count){
    return 0;
}

int main(int argc, char* argv[]){
    if(argc == 1){
        fprintf(stderr,"Programul nu a primit calea catre niciun director.\n");
        exit(-1);
    }
    else{
        int VariabilaModificare = 0;

        for(int i = 1; i < argc; i++){ //pentru fiecare director in parte
            DIR* director = deschideDirector(argv[i]); //calea catre director
            char cale[LUNGIME_NUME + 4];
            strcpy(cale,argv[i]);
            strcat(cale,".txt");

            int fd = deschideFisier(cale);

            //initializez structura care memoreaza informatiile citite
            Informatii* info = malloc(sizeof(Informatii)*MAX_SIZE_FISIER);
            if(!info){
                fprintf(stderr,"Eroare de alocare a memoriei.\n");
                exit(-1);
            }
            int* size = malloc(sizeof(int));
            if(!size){
                fprintf(stderr,"Eroare de alocare a memoriei.\n");
                exit(-1);
            }
            *size = 0;

            //citesc datele din director
            citesteDirector(director,argv[i],info,size);
            info = realloc(info,sizeof(Informatii)*(*size));
            if(!info){
                fprintf(stderr,"Eroare de alocare a memoriei.\n");
                exit(-1);
            }

            //verif citire corecta
            /*
            for(int j = 0; j < *size; j++){
                printf("%d %s %s\n",j,info[j].nume,info[j].modt);
            }
            */
            
            //imi creez un buffer de cautare in fisier, pe care il voi folosi si pentru a scrie datele in fisier la final
            char* buffer = malloc(sizeof(char) * 4096);
            strcpy(buffer,"");
            for(int k = 0; k < *size; k++){
                strcat(buffer,info[k].nume);
                strcat(buffer," ");
                strcat(buffer,info[k].modt);
                strcat(buffer,"\n");
            }

            //daca fisierul nu este gol, verific daca au aparut modificari si afisez rezultatul
            //daca fisierul este gol, memorez datele curente despre director in fisier si trec mai departe; se poate spune ca directorul nu a fost modificat
            if(!verificaFisierGol(cale)){  
                verificaSchimbare(info,fd,strlen(buffer));
            }

            //la final, ma asigur ca modific datele actualizate pentru fiecare fisier

            int verif = write(fd,buffer,strlen(buffer));
            if(verif != strlen(buffer)){
                fprintf(stderr,"Eroare de scriere in fisierul %s",cale);
                exit(-1);
            }
        }
    }

    return 0;
}
