#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

typedef struct{
    char nume[256];
    time_t modt;
}Informatii;

void citesteDirector(DIR* director, char* cale){
    struct dirent* aux;

    while((aux = readdir(director))){
        if(strcmp(aux->d_name,".") && strcmp(aux->d_name,"..")){
            if(aux->d_type == DT_DIR){
                DIR* d;
                char* nume = malloc(sizeof(char)*(strlen(cale)+1+strlen(aux->d_name)));
                strcpy(nume,cale);
                strcat(nume,"/");
                strcat(nume,aux->d_name);

                d = opendir(nume);
                if(!d){
                    fprintf(stderr,"Nu am putut deschide un director interior.\n");
                    exit(-1);
                }
                citesteDirector(d,nume);
            }
            struct stat st;
            if(lstat(cale,&st) == -1){
                fprintf(stderr,"Eroare de lstat.\n");
                exit(-1);
            }
        }        
    }
    if(errno != 0){
        fprintf(stderr,"Eroare la citirea din director.\n");
        exit(-1);
    }
}

int main(int argc, char* argv[]){
    if(argc != 3){
        fprintf(stderr,"Nu ati apelat cu 3 argumente!\n");
        exit(-1);
    }

    DIR* director;
    if(!(director = opendir(argv[1]))){
        fprintf(stderr,"Calea catre director nu este corecta/directorul nu s-a putut deschide.\n");
        exit(-1);
    }
    //initializez structura
    citesteDirector(director,argv[1]);
    FILE* f;
    if(!(f = fopen("r+",argv[2]))){
        fprintf(stderr,"Eroare la deschiderea fisierului de verificare.\n");
        exit(-1);
    }
    if(!sizeof(f)){
        //verifica daca e gol
    }
    //verificaSchimbare()

    return 0;
}
